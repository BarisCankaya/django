from django.shortcuts import render, HttpResponse, redirect
from app.forms import UserForm
from app.biz.userBO import UserBO
from django.contrib import messages
from app.biz.UserResult import *
from django.views.decorators.csrf import csrf_exempt
@csrf_exempt
def create(request):
    form=UserForm(request.POST)
    userBO=UserBO()
    userResult=UserResult()
    if request.method=="POST":
       if form.is_valid():
            form = UserForm(request.POST)
            userResult.height = request.POST.get('height')
            userResult.weight = request.POST.get('weight')
            userResult.shoeSize = request.POST.get('shoeSize')
            userResult=userBO.Control(userResult)
            messages.success(request,userResult.gender)
            messages.success(request,userResult.message)
            return HttpResponse(userResult.jsonResult, content_type="application/json")
    else:
        form=UserForm()
    return render(request,'app/index.html',{'form':form})
def readJsonFile(request):
    jsonResult=open('user.json','r').read()
